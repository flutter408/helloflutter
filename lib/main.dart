import 'package:flutter/material.dart';

void  main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String frenchGreeting = "Bonjour Flutter";
String portugueseGreeting = "Olá Flutter";
String hindiGreeting = "Namaste Flutter";
String chineseGreeting = "Ni hao Flutter";
class _MyStatefulWidgetState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;
    @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != spanishGreeting? spanishGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.add_box)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != frenchGreeting? frenchGreeting : portugueseGreeting;
                  });
                },
                icon: Icon(Icons.account_tree)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != hindiGreeting? hindiGreeting : chineseGreeting;
                  });
                },
                icon: Icon(Icons.airplay_sharp))
          ],

        ),
        body: Center (
          child: Text(
            displayText,style: TextStyle(fontSize: 26),
          ),
        ),
      ),
    );
  }
}



















// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center (
//           child: Text(
//             "Hello Flutter",style: TextStyle(fontSize: 26),
//           ),
//         ),
//       ),
//     );
//   }
// }
